import {memo} from "react";
import styles from './AdMetricColumn.module.css'
import Plot from "./Plot";
import MetricList from "./MetricList";

function AdMetricColumn({
        title,
        prefix = '',
        suffix = '',
        decimals = 0,
        meta,
        data,
        config: {isHidden, showValue, showPlot} = {isHidden: false, showValue: true, showPlot: true}
}) {

  const formatter = str => `${prefix}${parseFloat(str).toFixed(decimals)}${suffix}`;
  if (isHidden) {
    return <></>;
  }

  return (
    <div className={styles.container}>
      <h2>{title}</h2>
      <div className={styles.columns}>
        <MetricList data={meta} />
        <MetricList data={data} show={showValue} formatter={formatter} />
        <Plot data={data} show={showPlot} decimals={decimals} formatter={formatter} />
      </div>
    </div>
  );
}

export default memo(AdMetricColumn)