import styles from "./Plot.module.css";
import {memo} from "react";

function Plot({show, data = [], decimals = 0, formatter = data => data, width = 200, dotSize = 8}) {
  const plotMin = Math.min(...data).toFixed(decimals);
  const plotMax = Math.max(...data).toFixed(decimals);
  const plotValue = plotMax - plotMin;
  if (!show || data.length === 0) {
    return <></>
  }
  return (
    <div className={[styles.column, styles.plot].join(' ')}>
      <div>
        <div className={styles.plotLabels}>
          <span className={styles.min} style={{left: '-' + (`${plotMin}`.length * 5) + 'px'}}>{formatter(plotMin)}</span>
          <span className={styles.max} style={{left: (`${plotMax}`.length * 5) + 'px'}}>{formatter(plotMax)}</span>
        </div>
        {
          data.map((value, i) => {
            const currentValue = value - plotMin;
            const plotPercentDecimal = currentValue / plotValue;
            const plotPercent = plotPercentDecimal * (width - dotSize);
            return (
              <div key={i} className={styles.plot} style={{width: width + 'px'}}>
                <div className={styles.dot} style={{height: dotSize + 'px', width: dotSize + 'px', marginLeft: plotPercent + 'px'}}/>
              </div>
            )
          })
        }
      </div>
    </div>
  )
}

export default memo(Plot)