import React, {memo} from "react";
import styles from './AdMetrics.module.css'
import AdMetricColumn from "./AdMetricColumn";

function AdMetrics({query = {}, config}) {
  const {headers = [], meta, data} = query;
  return (
    <div className={styles.container}>
        <div className={styles.columns}>
          {
            headers.map(({key, ...rest}) => (
              <AdMetricColumn key={key}
                              {...rest}
                              meta={meta[key]}
                              data={data[key]}
                              config={config[key]}
              />
            ))
          }
        </div>
    </div>
  )
}

export default memo(AdMetrics)