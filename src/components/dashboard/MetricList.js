import styles from "./MetricList.module.css";
import {memo} from "react";

function MetricList({show = true, data = [], formatter = data => data}) {
  if (!show || data.filter(d => !!d).length === 0) {
    return <></>
  }
  return (
    <div className={styles.column}>
      <div>&nbsp;</div>
      { data.map((datum, i) => <div className={styles.row} key={i}>{formatter(datum) || '&nbsp;'}</div>) }
    </div>
  );
}

export default memo(MetricList)