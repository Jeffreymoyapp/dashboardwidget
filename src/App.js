import './App.css';
import useFetchAdMetrics from "./hooks/useFetchAdMetrics";
import AdMetrics from "./components/dashboard/AdMetrics";

function App() {
  const {query, config1, config2} = useFetchAdMetrics('https://w4m9n4r9.stackpathcdn.com/frontend-test-data.json')

  return (
    <div className="App">
      <AdMetrics query={query} config={config1} />
      <AdMetrics query={query} config={config2} />
    </div>
  );
}

export default App;
