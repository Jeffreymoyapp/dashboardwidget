import {useEffect, useState} from "react";

function useFetchAdMetrics(url) {
  const [data, setData] = useState({})
  useEffect(() => {
    fetch(url)
      .then(response => response.json())
      .then(json => {
        const {data: {query = {headers: [], meta: [], data: []}} = {}, config1 = [], config2 = []} = json;

        setData({
          config1: transform(config1, query.headers),
          config2: transform(config2, query.headers),
          query: {
            headers: query.headers,
            meta: transform(query.meta, query.headers, true),
            data: transform(query.data, query.headers, true)
          }

        });
      })
  }, [url]);
  return data;
}

function transform(data, headers, isArrValue) {
  const cols = headers.reduce((o, h) => ({[h.key]: [], ...o}), {});
  const out = {...cols};
  data.forEach(datum => {
    if (isArrValue) {
      if (datum instanceof Array) {
        const row = datum.reduce((o, kv) => ({[kv.k]: kv.v, ...o}), {});
        Object.keys(out).forEach(k => out[k].push(row[k] || ''))
      } else {
        out[datum.key].push(datum.title);
      }
    } else if(out[datum.key]) {
      out[datum.key] = datum;
    }
  });
  return out;
}

export default useFetchAdMetrics;
